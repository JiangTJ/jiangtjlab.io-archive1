---
title: 工具集
date: 2018-11-12 12:10:12
reward: false
comments: false
---

# 编辑器
- Typora  markdown编辑器
- VS Code  代码编辑器
- 石墨文档 协作文档  
- OneNote 微软出的笔记应用
- IntelliJ IDEA java开发工具

# 设计
- 墨刀  原型模型设计
- 亿图 流程图软件，试用过期了还能用
- Sway 微软出的故事线应用

# 其他
- Snipaste  截图工具(出了Win10商店版)
- Git2Go ios git client



